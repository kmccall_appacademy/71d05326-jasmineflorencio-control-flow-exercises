# EASY

# Return the argument with all its lowercase characters removed.
def destructive_uppercase(str)
  lowercase_alphabet = ("a".."z").to_a.join
  str.delete!(lowercase_alphabet)
end

# Return the middle character of a string. Return the middle two characters if
# the word is of even length, e.g. middle_substring("middle") => "dd",
# middle_substring("mid") => "i"
def middle_substring(str)
  middle_idx = str.length / 2
  if str.length.even?
    return str[middle_idx - 1] + str[middle_idx]
  else
    return str[middle_idx]
  end
end

# Return the number of vowels in a string.
VOWELS = %w(a e i o u)
def num_vowels(str)
  str.count("aeiou")
end

# Return the factoral of the argument (num). A number's factorial is the product
# of all whole numbers between 1 and the number itself. Assume the argument will
# be > 0.
def factorial(num)
  factorial_array = (1..num).to_a
  factorial_array.reduce(:*)
end


# MEDIUM

# Write your own version of the join method. separator = "" ensures that the
# default seperator is an empty string.
def my_join(arr, separator = "")
  joined_sentence = ""
  arr.each_with_index do |el, idx|
    joined_sentence << el
    next if idx == arr.length - 1
    joined_sentence << separator
  end
  joined_sentence
end

# Write a method that converts its argument to weirdcase, where every odd
# character is lowercase and every even is uppercase, e.g.
# weirdcase("weirdcase") => "wEiRdCaSe"
def weirdcase(str)
  weirdcase_str = ""
  str.each_char.with_index do |el, idx|
    if idx.even?
      weirdcase_str << el.downcase
    else
      weirdcase_str << el.upcase
    end
  end
  weirdcase_str
end

# Reverse all words of five more more letters in a string. Return the resulting
# string, e.g., reverse_five("Looks like my luck has reversed") => "skooL like
# my luck has desrever")
def reverse_five(str)
  reverse_str = []
  str.split(" ").each do |el|
    if el.length >= 5
      reverse_str << el.reverse
    else
      reverse_str << el
    end
  end
  reverse_str.join(" ")
end

# Return an array of integers from 1 to n (inclusive), except for each multiple
# of 3 replace the integer with "fizz", for each multiple of 5 replace the
# integer with "buzz", and for each multiple of both 3 and 5, replace the
# integer with "fizzbuzz".
def fizzbuzz(n)
  fizzbuzz_arr = []
  (1..n).to_a.each do |el|
    if el % 3 == 0 && el % 5 == 0
      fizzbuzz_arr << "fizzbuzz"
    elsif el % 3 == 0
      fizzbuzz_arr << "fizz"
    elsif el % 5 == 0
      fizzbuzz_arr << "buzz"
    else
      fizzbuzz_arr << el
    end
  end
  fizzbuzz_arr
end


# HARD

# Write a method that returns a new array containing all the elements of the
# original array in reverse order.
def my_reverse(arr)
  reverse_arr = []
  arr.each do |el|
    reverse_arr.unshift(el)
  end
  reverse_arr
end

# Write a method that returns a boolean indicating whether the argument is
# prime.
def prime?(num)
  count = 0
  (1..num).to_a.each do |el|
    if num % el == 0
      count += 1
    else
      next
    end
  end
  return true if count == 2
  false
end

# Write a method that returns a sorted array of the factors of its argument.
def factors(num)
  factors_arr = []
  (1..num).to_a.each do |el|
    if num % el == 0
      factors_arr << el
    else
      next
    end
  end
  factors_arr
end

# Write a method that returns a sorted array of the prime factors of its argument.
def prime_factors(num)
  prime_factor_arr = []
  factors(num).each do |el|
    if prime?(el)
      prime_factor_arr << el
    end
  end
  prime_factor_arr
end

# Write a method that returns the number of prime factors of its argument.
def num_prime_factors(num)
  prime_factors(num).count
end


# EXPERT

# Return the one integer in an array that is even or odd while the rest are of
# opposite parity, e.g. oddball([1,2,3]) => 2, oddball([2,4,5,6] => 5)
def oddball(arr)
  even_arr = []
  odd_arr = []
  arr.each do |el|
    if el.even?
      even_arr << el
    else
      odd_arr << el
    end
  end
  return even_arr[0] if even_arr.length == 1
  odd_arr[0]
end
